import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false
//引入封装好了的element-ui
// import elementui from '@/elementui'
// Vue.use(elementui)
//全局引入element-ui
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI)
//引入全局样式
// import '@/assets/css/common.css'
// import '@/assets/css/index.css'
// import '@/assets/css/login.css'
// import '@/assets/css/page.css'

//引入vuex仓库
import store from '@/store';

//引入VueRouter
// import VueRouter from 'vue-router'

// Vue.use(VueRouter)

//引入路由
import router from '@/router'

import { isValidUsername, isExternal, isCellPhone, checkUserName, checkName, checkPhone, validID, requestUrlParam } from '@/assets/js/validate'
Vue.prototype.$validate = {
  isValidUsername,
  isExternal,
  isCellPhone,
  checkUserName,
  checkName,
  checkPhone,
  validID,
  requestUrlParam
}

new Vue({
  render: h => h(App),
  //挂载路由
  router,
  //挂载vuex仓库
  store
}).$mount('#app')
