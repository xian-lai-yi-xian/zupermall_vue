import Vue from 'vue';
import Vuex from 'vuex';
Vue.use(Vuex);


//引入模块
import home from './home';
export default new Vuex.Store({
    //实现Vue仓库模块式管理
    modules: {
        home
    }
})