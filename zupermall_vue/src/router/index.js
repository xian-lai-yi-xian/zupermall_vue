//配置路由的主文件
import Vue from 'vue'
import Router from 'vue-router'

//引入路由组件
import Member from '@/page/member'
import MemberAdd from '@/page/member/add'
import Category from '@/page/category'
import Login from '@/page/login'
import Home from '@/page/home'
import Food from '@/page/food'
import FoodAdd from '@/page/food/add'
import Combo from '@/page/combo'
import ComboAdd from '@/page/combo/add'
import Order from '@/page/order'



//使用插件
Vue.use(Router);
//备份Router原型对象的push方法
let originPush = Router.prototype.push;
// console.log(originPush);

//备份Router原型对象的replace方法
let originReplace = Router.prototype.replace;

// 编程式路由跳转到当前路由（参数不变），多次执行会抛出NavigationDuplicated的错误警告
Router.prototype.push = function (location, resolve, reject) {
    if (resolve && reject) {
        return originPush.call(this, location, resolve, reject);
    } else {
        return originPush.call(this, location, () => {
        }, () => {
        });
    }
}
Router.prototype.replace = function (location, resolve, reject) {
    if (resolve && reject) {
        return originReplace.call(this, location, resolve, reject);
    } else {
        return originReplace.call(this, location, () => {
        }, () => {
        });
    }
}


//创建一个路由
export default new Router({
    routes: [
        {
            path: '/',
            redirect: '/home'
        },
        {
            path: '/home',
            component: Home,
            children: [
                //默认展示第一个
                {
                    path: '',
                    redirect: 'member'
                },
                {
                    name: 'member',
                    path: 'member',
                    component: Member,
                },
                {
                    name: 'memberadd',
                    path: 'memberadd',
                    component: MemberAdd,
                },
                {
                    name: 'category',
                    path: 'category',
                    component: Category,
                },
                {
                    name: 'food',
                    path: 'food',
                    component: Food,
                },
                {
                    name: 'foodAdd',
                    path: 'foodadd',
                    component: FoodAdd,
                },
                {
                    name: 'combo',
                    path: 'combo',
                    component: Combo,
                },
                {
                    name: 'comboAdd',
                    path: 'comboadd',
                    component: ComboAdd,
                },
                {
                    name: 'order',
                    path: 'order',
                    component: Order,
                },
            ]
        },
        {
            name: 'login',
            path: '/login',
            component: Login,
        },
    ]
})